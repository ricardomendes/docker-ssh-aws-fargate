#!/bin/sh

printTestHeader() {
    echo "$1"
    echo '----------'
}

printTestFooter() {
    echo '----------'
    echo 'Done!'
    echo
}

echo

# ---------------------------------------------------------------------------- #
# Verify `root` is the active user.                                            #
# ---------------------------------------------------------------------------- #
printTestHeader 'Checking the curent user (should be root)...'
echo $(whoami)
[ $(whoami) = 'root' ] || exit 1
printTestFooter
